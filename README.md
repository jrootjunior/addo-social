# Test application for Addo.UA
Simple REST API based social network in Django, and a
bot which demonstrate functionalities of the system.

## Contents
* [Notice](#notice)
* [Note](#note)
* [Installation](#installation)
    * [Database](#database)
    * [Django](#django)
        * [Preparing](#preparing)
        * [Start web server](#start-web-server)
    * [Bot](#bot)
        * [Preparing bot](#preparing-bot)
        * [Start web server](#starting)
* [Annotations (RU)](annotations.md)

## Notice
Python 3.6 is required! (In code used f-strings)

This manual is primarily written for Linux users.
To run the project on other OSs, the instruction may be different.

## Note:
If you see file with the prefix 'example.' in filename - remember
you need to clone that file without that prefix and configure it.

That is custom config files.

## Installation
 - Create virtualenv: `virtualenv env`
 - Activate virtual env: `source env/bin/activate`
 - Install requirements: `pip install -r requirements.txt`

### Database
 - Install [PostgreSQL](https://www.postgresql.org/download/)
 - Switch to postgres user (`sudo su - postgres`)
 - Create user: `createuser -DRSP addo_social` with password `mysupersecretpassword`
 - Create database: `createdb -O addo_social addo_social`

 > Is not recommended to store DB config in project repository but that is only test application.

### Django
Located at `src/`

#### Preparing:
- Activate virtual env (if not activated): `source env/bin/activate`
- Go to source dir: `cd src`
- Clone example config: `cp addo/example.conf.py addo/conf.py`
- Edit config (for e.g. in `nano`): `addo/conf.py`
- Apply DB migrations: `./manage.py migrate`

#### Start web server
`./manage.py runserver 8000`

### Bot
Located at `bot/`

Entry point is `main.py`

#### Preparing bot
- Activate virtual env (if not activated): `source env/bin/activate`
- Go to source dir: `cd bot`
- Clone example config: `cp example.conf.py conf.py`
- Edit config (for e.g. in nano): `nano conf.py`

#### Starting
- Execute bot `python3 main.py` or `./main.py`
