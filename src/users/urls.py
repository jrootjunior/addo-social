from django.conf.urls import url
from rest_framework_jwt import views as jwt_views

from . import views

urlpatterns = [
    url(r'^login$', jwt_views.obtain_jwt_token),
    url(r'^check$', jwt_views.verify_jwt_token),
    url(r'^signup$', views.signup_view),
]
