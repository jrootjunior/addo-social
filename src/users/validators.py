from django.utils.translation import ugettext_lazy as _
from requests import HTTPError
from rest_framework import serializers

from addo import settings
from .utils import hunter


class HunterIOValidator:
    message = _('Enter a valid email address.')

    def __call__(self, value) -> str:
        if settings.USE_HUNTER:
            return self.check(value)
        return value

    def check(self, value) -> str:
        """
        Validate email via Hunter.io

        Note: email can be 'undeliverable', 'risky' and 'deliverable'
            Only 'undeliverable' isn't allowed because gmail addresses is 'risky'

        :param value:
        :return: cleaned email address
        """
        try:
            hunter_result = hunter.email_verifier(value)
        except HTTPError:
            raise serializers.ValidationError(self.message)

        if hunter_result.get('result') == 'undeliverable':
            raise serializers.ValidationError(self.message)

        return hunter_result.get('email')
