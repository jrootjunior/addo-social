import clearbit
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from addo import settings
from .utils import get_jwt_token
from . import validators

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    username = serializers.EmailField(validators=[
        validators.HunterIOValidator(),
        UniqueValidator(UserModel.objects.all(), _('This username is already taken!'))
    ])
    password = serializers.CharField(write_only=True)

    token = serializers.SerializerMethodField('get_jwt_token')

    def create(self, validated_data):
        payload = {
            'username': validated_data['username'],
            'email': validated_data['username']
        }

        if settings.USE_CLEARBIT:
            # Get firs and last names via Clearbit
            enrichment = clearbit.Enrichment.find(email=validated_data['username'])
            if enrichment and 'person' in enrichment and 'name' in enrichment['person']:
                payload['first_name'] = enrichment['person']['name']['givenName'].capitalize()
                payload['last_name'] = enrichment['person']['name']['familyName'].capitalize()

        user = UserModel.objects.create(**payload)
        user.set_password(validated_data['password'])
        user.save()

        return user

    def get_jwt_token(self, user):
        return get_jwt_token(user)

    class Meta:
        model = UserModel
        fields = ('username', 'password', 'token')
