from django.contrib.auth import get_user_model
from django.db import models

from posts.exceptions import LikeError

UserModel = get_user_model()


class Like(models.Model):
    """
    Likes model
    """
    user = models.ForeignKey(UserModel)

    @property
    def post(self):
        return self.post_set.all()[0]

    def __str__(self):
        return f"{self.user} like {self.post}"


class Post(models.Model):
    author = models.ForeignKey(UserModel)

    title = models.CharField(max_length=512)
    content = models.TextField()

    likes = models.ManyToManyField(Like)

    created_date = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, user, title, content):
        """
        Create new post

        :param user: author
        :param title:
        :param content:
        :return: post instance
        """
        post = cls(author=user, title=title, content=content)
        post.save()
        return post

    def get_likes(self) -> int:
        """
        Get count of likes

        :return: int
        """
        return self.likes.count()

    def like(self, user, allow_self=False):
        """
        Add like to post

        :param user:
        :param allow_self:
        """
        if not allow_self and user == self.author:
            raise LikeError('You can not like your own posts')

        if self.is_liked_by_user(user):
            raise LikeError('You already like that post!')

        like = Like(user=user)
        like.save()

        self.likes.add(like)
        self.save()

    def unlike(self, user):
        """
        Remove like from post
        :param user:
        """
        try:
            like = self.likes.get(user=user)
        except Like.DoesNotExist:
            raise LikeError('You are not liked that post.')
        else:
            self.likes.remove(like)

    def is_liked_by_user(self, user) -> bool:
        """
        Check: User like that post?

        :param user:
        :return:
        """
        try:
            self.likes.get(user=user)
        except Like.DoesNotExist:
            return False
        return True

    def __str__(self):
        return f"Post ID:{self.pk} by {self.author} with title '{self.title}'"
