from rest_framework import status
from rest_framework.exceptions import APIException


class LikeError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Something went wrong."
    default_code = "like_error"
