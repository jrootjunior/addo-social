from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.posts_list_view),
    url(r'^(?P<pk>[0-9]+)/$', views.post_detailed),
    url(r'^(?P<pk>[0-9]+)/like$', views.post_like),
    url(r'^(?P<pk>[0-9]+)/unlike$', views.post_unlike)
]
