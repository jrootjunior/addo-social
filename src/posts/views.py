from rest_framework import permissions, viewsets
from rest_framework.response import Response

from . import models
from . import serializers


class PostsView(viewsets.ModelViewSet):
    """
     A viewset that provides default `create()`, `retrieve()`, `update()`,
    `partial_update()`, `destroy()`, `list()`, `like()` and `unlike()` actions.
    """

    queryset = models.Post.objects.all()
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.PostsSerializer

    def like(self, request, *args, **kwargs):
        post = self.get_object()
        post.like(request.user, True)
        return Response({'success': True})

    def unlike(self, request, *args, **kwargs):
        post = self.get_object()
        post.unlike(request.user)
        return Response({'success': True})

    def get_queryset(self):
        queryset = super(PostsView, self).get_queryset()

        # Provide to get posts of specific user
        author_id = self.request.query_params.get('author_id')
        # Provide to get posts with 0 likes
        no_liked = self.request.query_params.get('no_liked', 'false').lower() == 'true'
        # Can hide own posts
        hide_own = self.request.query_params.get('hide_own', 'false').lower() == 'true'

        if author_id is not None:
            queryset = queryset.filter(author=author_id)
        if no_liked:
            queryset = queryset.filter(likes=None)
        if hide_own:
            queryset = queryset.exclude(author=self.request.user)

        return queryset


posts_list_view = PostsView.as_view({
    'get': 'list',
    'post': 'create'
})

post_detailed = PostsView.as_view({
    'get': 'retrieve',
    'delete': 'destroy'
})

post_like = PostsView.as_view({
    'post': 'like'
})

post_unlike = PostsView.as_view({
    'post': 'unlike'
})
