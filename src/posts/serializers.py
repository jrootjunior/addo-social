from rest_framework import serializers

from posts.models import Post
from . import models


class PostsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    author_id = serializers.SerializerMethodField('get_post_author_id', read_only=True)
    author_name = serializers.SerializerMethodField('get_post_author_name', read_only=True)

    created_date = serializers.DateTimeField(read_only=True)

    title = serializers.CharField(max_length=512)
    content = serializers.CharField()

    likes = serializers.SerializerMethodField('get_likes_count')
    liked = serializers.SerializerMethodField('get_is_liked')

    @property
    def user(self):
        return self.context['request'].user

    def create(self, validated_data):
        post = Post.create(self.user, validated_data['title'], validated_data['content'])
        return post

    def get_post_author_id(self, post):
        return post.author.pk

    def get_post_author_name(self, post):
        name = ''
        if post.author.first_name:
            name += post.author.first_name
        if post.author.last_name:
            name += (' ' if name else '') + post.author.last_name

        if not name:
            name = post.author.username
        return name

    def get_likes_count(self, post):
        return post.get_likes()

    def get_is_liked(self, post):
        return post.is_liked_by_user(self.user)

    class Meta:
        model = models.Post
        fields = ('id', 'author_id', 'author_name', 'title', 'content', 'created_date', 'likes', 'liked')
