import typing

from requests import Response


class ApiException(Exception):
    """
    Base class for all API errors
    """

    def __init__(self, response: Response, *args):
        self.response: Response = response
        super(ApiException, self).__init__(*args)

    @property
    def status_code(self) -> int:
        return self.response.status_code

    @property
    def json(self) -> typing.Dict:
        return self.response.json()
