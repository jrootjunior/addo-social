import typing


class HTTPMethods:
    """
    HTTP Methods helper
    """

    GET = 'GET'
    POST = 'POST'


def generate_payload(data: typing.Dict, exclude: typing.List[str] = None) -> typing.Dict:
    """
    Generate payload from data

    >>> foo = 'foo'
    >>> bar = 42
    >>> baz = None
    >>> payload = generate_payload(locals(), exclude=['foo'])
    <<< {'bar': 42}

    :param data:
    :param exclude:
    :return: payload
    """
    if exclude is None:
        exclude = []
    if 'self' not in exclude:
        exclude.append('self')

    return {k: v for k, v in data.items() if k not in exclude and v is not None}


class ApiMethods:
    """
    Api methods helper
    """

    # Users management
    LOGIN = (HTTPMethods.POST, 'auth/login')
    SIGNUP = (HTTPMethods.POST, 'auth/signup')
    CHECK_TOKEN = (HTTPMethods.POST, 'auth/check')

    # Posts
    POSTS_LIST = (HTTPMethods.GET, 'posts/')
    CREATE_POST = (HTTPMethods.POST, 'posts/')
    READ_POST = (HTTPMethods.GET, 'posts/{pk}')
    LIKE_POST = (HTTPMethods.POST, 'posts/{pk}/like')
    UNLIKE_POST = (HTTPMethods.POST, 'posts/{pk}/unlike')
