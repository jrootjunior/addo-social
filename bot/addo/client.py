import logging
import typing
from http import HTTPStatus

import requests

from . import types
from .exceptions import ApiException
from .utils import ApiMethods
from .utils import generate_payload

DEFAULT_ADDRESS = 'http://localhost:8000'
DEFAULT_API_VERSION = 1

log = logging.getLogger(__name__)


class AddoSocial:
    """
    Base client class
    """

    def __init__(self, name, address=DEFAULT_ADDRESS, api_version=DEFAULT_API_VERSION):
        """
        :param name: is needed only for visual identification of object
        :param address: base URL
        :param api_version:
        """
        self.name = name
        self.address = address.rstrip('/')
        self.api_version = api_version

        self.session = requests.Session()
        self.headers = {}
        self._jwt = None

    def _set_token(self, token):
        """
        Save JWT token

        :param token:
        :return:
        """
        self._jwt = token
        self.headers['Authorization'] = f"JWT {token}"

    def _get_url(self, path) -> str:
        """
        Generate endpoint URL

        :param path:
        :return: URL
        """
        return f"{self.address}/api/v{self.api_version}/{path.lstrip('/')}"

    def raw_request(self, method: str, url: str,
                    params: typing.Optional[typing.Dict] = None,
                    payload: typing.Optional[typing.Dict] = None,
                    **kwargs) -> typing.Union[typing.List, typing.Dict]:
        """
        Make an request

        :param method:
        :param url:
        :param params:
        :param payload:
        :param kwargs:
        :return:
        """
        if params is None:
            params = {}
        if payload is None:
            payload = {}

        if kwargs:
            url = url.format(**kwargs)

        req = self.session.request(method, url, headers=self.headers, data=payload, params=params)

        log.debug(f"Endpoint: {req.url} -> Response({req.status_code}, {req.text})")

        if HTTPStatus.OK <= req.status_code <= HTTPStatus.IM_USED:
            return req.json()

        raise ApiException(req, f"[{req.status_code}] {req.content}")

    def request(self, method: str, path: str,
                params: typing.Optional[typing.Dict] = None,
                payload: typing.Optional[typing.Dict] = None,
                **kwargs) -> typing.Union[typing.List, typing.Dict]:
        """
        Make an API request

        :param method:
        :param path:
        :param params:
        :param payload:
        :param kwargs:
        :return:
        """
        url = self._get_url(path)
        return self.raw_request(method, url, params, payload, **kwargs)

    def signup(self, username, password, auth=True) -> typing.Dict:
        """
        Register new user

        :param username: valid email
        :param password:
        :param auth: save JWT token
        :return: dict with JWT token
        """
        payload = generate_payload(locals(), exclude=['auth'])
        response = self.request(*ApiMethods.SIGNUP, payload=payload)
        if auth:
            self._set_token(response['token'])
        return response

    def login(self, username, password, auth=True) -> str:
        """
        Authenticate user

        :param username: email
        :param password:
        :param auth: save JWT token
        :return: JWT token
        """
        payload = generate_payload(locals(), exclude=['auth'])
        response = self.request(*ApiMethods.LOGIN, payload=payload)
        token = response['token']
        if auth:
            self._set_token(token)
        return token

    def check_token(self, token=None):
        """
        Check JWT token

        :param token:
        :return: JWT token
        """
        if token is None:
            token = self._jwt

        payload = generate_payload(locals())
        response = self.request(*ApiMethods.CHECK_TOKEN, payload=payload)
        return response.get('token')

    def create_post(self, title, content) -> types.Post:
        """
        Write post

        :param title:
        :param content:
        :return: post object
        :rtype: types.Post
        """
        payload = generate_payload(locals())

        response = self.request(*ApiMethods.CREATE_POST, payload=payload)
        return types.Post.deserialize(response)

    def posts_list(self,
                   author_id: typing.Optional[int] = None,
                   hide_own: typing.Optional[bool] = None,
                   no_liked: typing.Optional[bool] = None,
                   limit: typing.Optional[int] = None,
                   offset: typing.Optional[int] = None) -> types.PaginatedResponse:
        """
        Get posts list

        :param author_id:
        :param hide_own: hide own posts
        :param no_liked: show only posts with 0 likes
        :param limit: records per page
        :param offset:
        :return: paginated response with posts
        :rtype: types.PaginatedResponse
        """
        payload = generate_payload(locals())
        response = self.request(*ApiMethods.POSTS_LIST, params=payload)
        return types.PaginatedResponse.deserialize(response, results_type=types.Post)

    def get_post(self, post_id) -> types.Post:
        """
        Get post by ID

        :param post_id:
        :return: post
        :rtype: types.Post
        """
        response = self.request(*ApiMethods.READ_POST, pk=post_id)
        return types.Post.deserialize(response)

    def like(self, post_id) -> bool:
        """
        Like post. You cannot like own posts or like if you already liked that post

        :param post_id:
        :return: return True if success otherwise raise ApiException
        """
        return self.request(*ApiMethods.LIKE_POST, pk=post_id).get('success')

    def unlike(self, post_id) -> bool:
        """
        Unlike post

        :param post_id:
        :return: return True if success otherwise raise ApiException
        """
        return self.request(*ApiMethods.UNLIKE_POST, pk=post_id).get('success')
