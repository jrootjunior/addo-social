import typing

from .utils import HTTPMethods


class Deserializable:
    """
    Base class for deserializable objects
    """

    @classmethod
    def de_json(cls, raw_data, **kwargs):
        """
        Deserialize object

        :param raw_data:
        :param kwargs:
        :return: instance of Deserializable object
        """
        return cls(**raw_data, **kwargs)

    @classmethod
    def deserialize(cls, raw_data, **kwargs):
        """
        Deserialize object or list of objects

        :param raw_data:
        :param kwargs:
        :return:
        """
        if isinstance(raw_data, list):
            return [cls.de_json(raw_obj, **kwargs) for raw_obj in raw_data]
        return cls.de_json(raw_data, **kwargs)


class PaginatedResponse(Deserializable):
    """
    This object used for representing paginated responses
    """

    def __init__(self, count, next, previous, results,
                 results_type: Deserializable, **kwargs):
        self.count: int = count
        self.next: typing.Optional[str] = next
        self.previous: typing.Optional[str] = previous
        self.results: results_type = results_type.deserialize(results)

        self.results_type = results_type

    def _get_page(self, client, url):
        if not url:
            raise ValueError('Page not found!')

        response = client.raw_request(HTTPMethods.GET, url)
        return self.__class__.deserialize(response, results_type=self.results_type)

    def get_next(self, client):
        """
        Get next page

        :param client:
        :return: page
        :rtype: PaginatedResponse
        """
        return self._get_page(client, self.next)

    def get_previous(self, client):
        """
        Get previous page

        :param client:
        :return: page
        :rtype: PaginatedResponse
        """
        return self._get_page(client, self.previous)


class Post(Deserializable):
    """
    This object used for representing posts objects as Python object
    """

    def __init__(self, id, author_id, author_name, title, content, created_date, likes, liked, **kwargs):
        self.id: int = id
        self.author_id: int = author_id
        self.author_name: str = author_name
        self.title: str = title
        self.content: str = content
        self.created_date: str = created_date
        self.likes: int = likes
        self.liked: int = liked
