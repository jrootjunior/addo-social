#!/usr/bin/env python
import itertools
import logging
import random
import sys
import typing

import faker

import conf
from addo.client import AddoSocial

logging.basicConfig(level=logging.INFO,
                    stream=sys.stdout,
                    format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')

log = logging.getLogger('bot')
fake = faker.Faker()


def signup_clients(count=1) -> typing.List[AddoSocial]:
    """
    Signup as new users with random username and passwords

    :return: list of clients
    :rtype: typing.List[AddoSocial]
    """
    clients = []
    for _ in range(count):
        username = fake.free_email()
        password = fake.password(length=16)
        client = AddoSocial(username)
        log.info(f"Signup as '{client.name}'")
        client.signup(username, password)
        clients.append(client)
    return clients


def create_posts(client: AddoSocial, count: int, words_in_title: int = 6, body_length: int = 512):
    """
    Create posts

    :param client:
    :param count: count of posts
    :param words_in_title: count of words in title
    :param body_length: length of body
    """
    for _ in range(count):
        title = fake.sentence(nb_words=words_in_title, variable_nb_words=True, ext_word_list=None)
        content = fake.text(max_nb_chars=body_length, ext_word_list=None)
        post = client.create_post(title, content)
        log.info(f"[{client.name}] Create post '{title}' (ID:{post.id})")


def like_posts(client: AddoSocial, count: int) -> int:
    """
    User activity:
    - next user to perform a like is the user who has most posts and has not reached max likes
    - user performs “like” activity until he reaches max likes
    - user can only like random posts from users who have at least one post with 0 likes
    - if there is no posts with 0 likes, bot stops
    - users cannot like their own posts
    - posts can be liked multiple times, but one user can like a certain post only once

    :param client:
    :param count: max likes
    :return: count of likes
    :rtype: int
    """
    # Get count of posts of other users where found zero likes
    posts_count = client.posts_list(hide_own=True, no_liked=True, limit=1).count

    # Get all posts. Is too long process.
    posts_list = client.posts_list(no_liked=True, hide_own=True, limit=posts_count).results
    user_with_no_liked_post = set(post.author_id for post in posts_list)

    liked = []

    for author_id in itertools.cycle(user_with_no_liked_post):
        log.info(f"[{client.name}] Fetch posts of UID:{author_id}...")
        # Get user posts
        user_posts = client.posts_list(author_id=author_id, limit=posts_count)
        posts_list = [post for post in user_posts.results if not post.liked]

        if not len(posts_list):
            # Try walk over pages
            while user_posts.next:
                user_posts = user_posts.get_next(client)
                posts_list = [post for post in user_posts.results if not post.liked]
                if posts_list:
                    break
            else:
                break

        # Select random posts
        random.shuffle(posts_list)
        if len(posts_list) > 1:
            posts_list = posts_list[:random.randint(1, min((len(posts_list), count - len(liked))))]

        # Like posts
        for post in posts_list:
            client.like(post.id)
            log.info(f"[{client.name}] [{len(liked) + 1}/{count}] "
                     f"Like post ID:{post.id} {post.author_name} '{post.title}'")
            liked.append(post)
            if len(liked) == count:
                break

        if len(liked) == count:
            break

    return len(liked)


def main():
    """
    In this function is realized main activity of automated bot.
    """
    if conf.number_of_users < 1:
        log.warning('Are you kidding me? No one user was not created because "number_of_users" must be grater than 0.')
        return

    log.info(f"Signup {conf.number_of_users} users.")
    clients = signup_clients(conf.number_of_users)

    log.info('Generate new posts...')
    if conf.max_posts_per_user > 0:
        for client in clients:
            create_posts(client, conf.max_posts_per_user, conf.words_in_title, conf.length_of_body)
        log.info(f"Successful generated {conf.number_of_users * conf.max_posts_per_user} posts "
                 f"by {conf.number_of_users} users.")
    else:
        log.warning('No one post is created. Parameter "max_posts_per_user" must be grater than 0.')

    log.info('Started liking the posts...')
    if conf.max_likes_per_user > 0:
        total_liked = 0
        for client in clients:
            liked = like_posts(client, conf.max_likes_per_user)
            total_liked += liked
            log.info(f"[{client.name}] Liked {liked}/{conf.max_likes_per_user} posts.")

            if 0 < liked < conf.max_likes_per_user:
                log.warning(f"[{client.name}] Not found more not liked posts for that user.")
            elif liked == 0:
                log.warning('There is no posts with 0 likes. Exit immediately.')
                break
        log.info(f"Totally liked {total_liked}/{conf.number_of_users * conf.max_likes_per_user} posts.")
    else:
        log.warning('No one post is liked. Parameter "max_likes_per_user" must be grater than 0.')


if __name__ == '__main__':
    main()
