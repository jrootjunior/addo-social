# Base settings
number_of_users = 5
max_posts_per_user = 10
max_likes_per_user = 25

# Post settings
words_in_title = 6
length_of_body = 128
